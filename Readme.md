# What2Eat Pour subvenir à vos besoin de votre cuisine

[Todo](./TODO.md)
 
[Miro Board](https://miro.com/app/board/uXjVPTKdvI4=/)



## Description

Nous avons décidé de nommer ce projet What2Eat. What2Eat est une application
mobile. Elle permet à son utilisateur de scanner les produits qu’il y a dans sa cuisine,
de les enregistrer sur son compte puis de se voir proposer des recettes possibles à
partir de ce qu’il possède. L’intérêt de cette application est d’éviter le gaspillage
alimentaire tout en découvrant de nouvelles recettes

Ce projet se tourne vers des technologies comme Dotnet, IONIC, Azure.

## Le cahier des charges fonctionnel

Dans cette application, un utilisateur peut enregistrer un compte. Avec ce même compte, il
pourra ajouter des ingrédients et des recettes avec son frigo virtuel.

## Le cahier des charges technique
● Une base de données Sql Server 2019
● Un Back écrit en Donet 6
● Un Front en Ionic 6
● Un Projet de Test en donet 6
● Un lancement de la base de données sur Azure

# Lancement
## Connection
### Compte
    antoine alpha_PWD
    thibaut beta_PWD

## Local
### BDD docker
- [To Readme BDD](./BDD/Readme.md)

### Back local
Mise à jours de la bdd avec dotnet ef

cd Back && dotnet ef database update

dotnet run

https://localhost:7073/swagger/index.html
### Front local

service AccessAPIService string


cd ../Front && ng serve

## DEV
### BDD
### Back
update connection string
cd Back dotnet ef database update
### Front

#### Connection Strings
    alpha_connection_DEV
    beta_connection_DEV

## INT
### BDD docker
#### Connection Strings
    alpha_connection_INT
    beta_connection_INT

## PROD
### BDD azure
#### Connection Strings
    alpha_connection_PRD
    beta_connection_PRD
#### RUN
.\BDD\azure\azureSqlServer.sh
cd Back dotnet ef database update
### BACK
#### RUN
dotnet run
https://localhost:7073/swagger/index.html
### FRONT
ng serve
http://localhost:4200/
login antoine/antoine

ionic capacitor build android --prod --no-open



npm install --save-dev @angular-devkit/build-angular
To build a Docker Image, we have to run the following command in our terminal:
docker build -t myApp:v1
To run the built docker image, use the following command:
docker run -d --name myAppContainer --network host myApp:v1

https://learn.microsoft.com/fr-fr/azure/app-service/tutorial-dotnetcore-sqldb-app?toc=%2Faspnet%2Fcore%2Ftoc.json&bc=%2Faspnet%2Fcore%2Fbreadcrumb%2Ftoc.json&view=aspnetcore-7.0&tabs=azure-portal%2Cvisualstudio-deploy%2Cdeploy-instructions-azure-portal%2Cazure-portal-logs%2Cazure-portal-resources
# Autre
;Integrated Security=true;

# pourquoi la solution docker compose n'est pas viable
le depond_on lance seulement il attend pas la fin d'execution