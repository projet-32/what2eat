﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Data;
using API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace What2EatMSTest
{
    public class TestDatabaseFixture
    {
        private const string ConnectionString = "server=localhost\\sqlexpress;database=what2eat;trusted_connection=true";

        private static readonly object _lock = new();
        private static bool _databaseInitialized;

        public TestDatabaseFixture()
        {
            lock (_lock)
            {
                if (!_databaseInitialized)
                {
                    using (var context = CreateContext())
                    {
                        context.Database.EnsureDeleted();
                        context.Database.EnsureCreated();
                        /*
                        context.AddRange(
                            new Utilisateur { Id = 777, Prenom = "Teste", Nom = "M_Teste", ImageId = 1, MotDePasse = "mot_de_passe", MonSecret = "mon_secret" });
                        context.SaveChanges();*/ 
                    }

                    _databaseInitialized = true;
                }
            }
        }

        public DataContext CreateContext()
            => new DataContext(
            new DbContextOptionsBuilder<DataContext>()
                .UseSqlServer(ConnectionString)
                .Options);
    }
}
