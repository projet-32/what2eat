#az account set -s $subscription # ...or use 'az login'
az login
# Création de la base de données
location="westeurope"
randomIdentifier="what2eat"
resource="$randomIdentifier-resource"
sqlserver="$randomIdentifier-sqlserver" ## modifier sqlserver
database="$randomIdentifier-database"
plan="$randomIdentifier-plan"
webapp="$randomIdentifier-webapp"
staticwebapp="$randomIdentifier-static-web-app"
login="antoine"
password="alpha_PWD"
startIP=0.0.0.0
endIP=255.0.0.0

# Use 'az account list-locations --output table' to list available locations close to you
# Create a resource group
az group create --name $resource --location $location

az appservice plan create --name $plan --resource-group $resource --sku F1

az webapp create --name $webapp --runtime "DOTNET|6.0" --plan $plan --resource-group $resource

az sql server create --location $location --resource-group $resource --name $sqlserver --admin-user $login --admin-password $password

az sql db create --resource-group $resource --server $sqlserver --name $randomIdentifier

az webapp connection create sql --resource-group $resource --name $webapp --target-resource-group $resource --server $sqlserver --database $database --query configurations --secret name=$login secret=$password
    
az sql server firewall-rule create --resource-group $resource --server $sqlserver --name LocalAccess --start-ip-address $startIP --end-ip-address $endIP

$SHELL
# change Connection String
cd Back && dotnet ef database update
# /mnt/c/Users/Trafi/.dotnet/tools/dotnet-ef.exe database update

#az webapp deployment source config-local-git --name $webapp --resource-group $resource
#az webapp deployment list-publishing-credentials --name $webapp --resource-group $resource --query "{Username:publishingUserName, Password:publishingPassword}"
#git remote add azure https://$webapp.scm.azurewebsites.net/$webapp.git
#git config --global user.email "you@example.com"
#git config --global user.name "Velbi"
#git add . 
#git commit -m "Updating the modified appsettings.json and Startup.cs files"
#git push azure main:master

# https://$webapp.scm.$webapp.p.azurewebsites.net
# Photo dans publish
# error dowload https://what2eat-webapp.azurewebsites.net/api/dump
dotnet publish -c Release

az staticwebapp create \
    --name $staticwebapp \
    --resource-group $resource \
    --source https://gitlab.com/re-world/what2eat \
    --location $location \
    --branch main \
    --app-location "/Front" \
    --output-location "dist/angular-basic" \
    --token "glpat-ZhQ_hJtzGCKcB-ycWcM1"

keytool -genkey -v -keystore my-release-what2eat-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias what2eat-alias

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-what2eat-key.jks what2eat.apk what2eat-alias
# Configurer et diffuser en continu les journaux d’applications
az webapp log config \
    --web-server-logging 'filesystem' \
    --name $APP_SERVICE_NAME \
    --resource-group $RESOURCE_GROUP_NAME
# change access-api.service.ts Connection String
curl https://what2eat-webapp.azurewebsites.net/api/Utilisateur
https://what2eat-webapp.azurewebsites.net/swagger/index.html
az group delete --name $resource