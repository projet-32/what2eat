﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categorie",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nom = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorie", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    idImage = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    image_64 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    image_path = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.idImage);
                });

            migrationBuilder.CreateTable(
                name: "Ingredient",
                columns: table => new
                {
                    idIngredient = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nom = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    codeBarre = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    quantite = table.Column<int>(type: "int", nullable: false),
                    contenant = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: true),
                    unite = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Image_id = table.Column<int>(type: "int", nullable: true),
                    Categorie_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredient", x => x.idIngredient);
                    table.ForeignKey(
                        name: "fk_Ingredient_Categorie",
                        column: x => x.Categorie_id,
                        principalTable: "Categorie",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_Ingredient_Image",
                        column: x => x.Image_id,
                        principalTable: "Image",
                        principalColumn: "idImage");
                });

            migrationBuilder.CreateTable(
                name: "Recette",
                columns: table => new
                {
                    idRecette = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nom = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    temps = table.Column<int>(type: "int", nullable: true),
                    personne = table.Column<int>(type: "int", nullable: true),
                    Image_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recette", x => x.idRecette);
                    table.ForeignKey(
                        name: "fk_Recette_Image",
                        column: x => x.Image_id,
                        principalTable: "Image",
                        principalColumn: "idImage");
                });

            migrationBuilder.CreateTable(
                name: "Utilisateur",
                columns: table => new
                {
                    idUtilisateur = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nom = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    prenom = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    mon_secret = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    mot_de_passe = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Image_idImage = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utilisateur", x => x.idUtilisateur);
                    table.ForeignKey(
                        name: "fk_Utilisateur_Image",
                        column: x => x.Image_idImage,
                        principalTable: "Image",
                        principalColumn: "idImage");
                });

            migrationBuilder.CreateTable(
                name: "Etape",
                columns: table => new
                {
                    idEtape = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    description = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    position = table.Column<int>(type: "int", nullable: false),
                    Recette_id = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Etape", x => x.idEtape);
                    table.ForeignKey(
                        name: "fk_Etape_has_Recette",
                        column: x => x.Recette_id,
                        principalTable: "Recette",
                        principalColumn: "idRecette");
                });

            migrationBuilder.CreateTable(
                name: "Ingredient_has_Recette",
                columns: table => new
                {
                    Ingredient_id = table.Column<int>(type: "int", nullable: false),
                    Recette_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredient_has_Recette", x => new { x.Ingredient_id, x.Recette_id });
                    table.ForeignKey(
                        name: "fk_Ingredient_has_Recette_Ingredient",
                        column: x => x.Ingredient_id,
                        principalTable: "Ingredient",
                        principalColumn: "idIngredient");
                    table.ForeignKey(
                        name: "fk_Ingredient_has_Recette_Recette",
                        column: x => x.Recette_id,
                        principalTable: "Recette",
                        principalColumn: "idRecette");
                });

            migrationBuilder.CreateTable(
                name: "Favori",
                columns: table => new
                {
                    Utilisateur_id = table.Column<int>(type: "int", nullable: false),
                    Recette_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Favori", x => new { x.Utilisateur_id, x.Recette_id });
                    table.ForeignKey(
                        name: "fk_Utilisateur_has_Recette_Recette",
                        column: x => x.Recette_id,
                        principalTable: "Recette",
                        principalColumn: "idRecette");
                    table.ForeignKey(
                        name: "fk_Utilisateur_has_Recette_Utilisateur",
                        column: x => x.Utilisateur_id,
                        principalTable: "Utilisateur",
                        principalColumn: "idUtilisateur");
                });

            migrationBuilder.CreateTable(
                name: "Frigo",
                columns: table => new
                {
                    Utilisateur_id = table.Column<int>(type: "int", nullable: false),
                    Ingredient_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Frigo", x => new { x.Ingredient_id, x.Utilisateur_id });
                    table.ForeignKey(
                        name: "fk_Utilisateur_has_Ingredient_Ingredient",
                        column: x => x.Ingredient_id,
                        principalTable: "Ingredient",
                        principalColumn: "idIngredient");
                    table.ForeignKey(
                        name: "fk_Utilisateur_has_Ingredient_Utilisateur",
                        column: x => x.Utilisateur_id,
                        principalTable: "Utilisateur",
                        principalColumn: "idUtilisateur");
                });

            migrationBuilder.CreateTable(
                name: "ListCourse",
                columns: table => new
                {
                    Utilisateur_id = table.Column<int>(type: "int", nullable: false),
                    Ingredient_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ListCourse", x => new { x.Utilisateur_id, x.Ingredient_id });
                    table.ForeignKey(
                        name: "fk_Utilisateur_has_Ingredient_Ingredient1",
                        column: x => x.Ingredient_id,
                        principalTable: "Ingredient",
                        principalColumn: "idIngredient");
                    table.ForeignKey(
                        name: "fk_Utilisateur_has_Ingredient_Utilisateur1",
                        column: x => x.Utilisateur_id,
                        principalTable: "Utilisateur",
                        principalColumn: "idUtilisateur");
                });

            migrationBuilder.InsertData(
                table: "Categorie",
                columns: new[] { "id", "nom" },
                values: new object[,]
                {
                    { 1, "Fruits" },
                    { 2, "Conserves" },
                    { 3, "Sodas" },
                    { 4, "Legumes" },
                    { 5, "Viandes" }
                });

            migrationBuilder.InsertData(
                table: "Image",
                columns: new[] { "idImage", "image_64", "image_path" },
                values: new object[,]
                {
                    { 1, "", "/Photos/paysage.jpg" },
                    { 2, "", "/Photos/groseille.jpg" },
                    { 3, "", "/Photos/pamplemousse.jpg" },
                    { 4, "", "/Photos/papaye.jpg" },
                    { 5, "", "/Photos/pasteque.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Ingredient",
                columns: new[] { "idIngredient", "Categorie_id", "codeBarre", "contenant", "Image_id", "nom", "quantite", "unite" },
                values: new object[,]
                {
                    { 1, 2, "", "Sachet", 1, "Tranche de Pain Hamburger", 4, "1" },
                    { 2, 3, "", "Sachet", 1, "Salade", 4, "1" },
                    { 3, 4, "", "Sachet", 1, "Tomates", 4, "1" },
                    { 4, 5, "", "Sachet", 1, "Steak", 4, "1" },
                    { 5, 1, "", "Sachet", 1, "Groseille", 7, "1" },
                    { 6, 1, "", "Sachet", 1, "Pamplemousse", 14, "1" },
                    { 7, 1, "", "Sachet", 1, "Papaye", 4, "1" },
                    { 8, 1, "", "Sachet", 1, "Pastèque", 9, "1" },
                    { 9, 1, "", "Sachet", 1, "Raisin", 4, "1" },
                    { 10, 3, "", "Bouteille", 1, "Coca Cola", 7, "1" },
                    { 11, 3, "", "Bouteille", 1, "Pepsi", 4, "1" },
                    { 12, 3, "", "Bouteille", 1, "S.Pellegrino", 4, "1" }
                });

            migrationBuilder.InsertData(
                table: "Recette",
                columns: new[] { "idRecette", "Image_id", "nom", "personne", "temps" },
                values: new object[] { 1, 1, "Hamburgers", 1, 15 });

            migrationBuilder.InsertData(
                table: "Utilisateur",
                columns: new[] { "idUtilisateur", "Image_idImage", "mon_secret", "mot_de_passe", "nom", "prenom" },
                values: new object[,]
                {
                    { 1, 1, "antoine", "antoine", "antoine", "antoine" },
                    { 2, 1, "secret", "azerty", "Alexander", "Carson" },
                    { 3, 1, "secret", "azerty", "Alonso", "Meredith" },
                    { 4, 1, "secret", "azerty", "Anand", "Arturo" },
                    { 5, 1, "secret", "azerty", "Barzdukas", "Gytis" },
                    { 6, 1, "secret", "azerty", "Li", "Yan" },
                    { 7, 1, "secret", "azerty", "Justice", "Peggy" },
                    { 8, 1, "secret", "azerty", "Norman", "Laura" },
                    { 9, 1, "secret", "azerty", "Olivetto", "Nino" }
                });

            migrationBuilder.InsertData(
                table: "Etape",
                columns: new[] { "idEtape", "description", "position", "Recette_id" },
                values: new object[,]
                {
                    { 1, "Poser dans une assiette une tranche de pain", 1, 1 },
                    { 2, "Poser ensuite une feuille de salade", 2, 1 },
                    { 3, "Poser délicatement le skeak précuit", 3, 1 },
                    { 4, "Poser ensuite une rondelle de tomate", 4, 1 },
                    { 5, "Recouvrer le tout part une autre tranche de pain", 5, 1 },
                    { 6, "Mettre au four 10 min", 6, 1 }
                });

            migrationBuilder.InsertData(
                table: "Frigo",
                columns: new[] { "Ingredient_id", "Utilisateur_id" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 4, 1 },
                    { 5, 1 },
                    { 6, 1 },
                    { 7, 1 },
                    { 8, 1 },
                    { 9, 1 },
                    { 10, 1 },
                    { 11, 1 },
                    { 12, 1 }
                });

            migrationBuilder.InsertData(
                table: "Ingredient_has_Recette",
                columns: new[] { "Ingredient_id", "Recette_id" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 4, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "fk_Etape_has_Recette_idx",
                table: "Etape",
                column: "Recette_id");

            migrationBuilder.CreateIndex(
                name: "fk_Utilisateur_has_Recette_Recette_idx",
                table: "Favori",
                column: "Recette_id");

            migrationBuilder.CreateIndex(
                name: "fk_Utilisateur_has_Recette_Utilisateur_idx",
                table: "Favori",
                column: "Utilisateur_id");

            migrationBuilder.CreateIndex(
                name: "fk_Utilisateur_has_Ingredient_Ingredient_idx",
                table: "Frigo",
                column: "Ingredient_id");

            migrationBuilder.CreateIndex(
                name: "fk_Utilisateur_has_Ingredient_Utilisateur_idx",
                table: "Frigo",
                column: "Utilisateur_id");

            migrationBuilder.CreateIndex(
                name: "fk_Ingredient_Categorie_idx",
                table: "Ingredient",
                column: "Categorie_id");

            migrationBuilder.CreateIndex(
                name: "fk_Ingredient_Image_idx",
                table: "Ingredient",
                column: "Image_id");

            migrationBuilder.CreateIndex(
                name: "fk_Ingredient_has_Recette_Ingredient_idx",
                table: "Ingredient_has_Recette",
                column: "Ingredient_id");

            migrationBuilder.CreateIndex(
                name: "fk_Ingredient_has_Recette_Recette_idx",
                table: "Ingredient_has_Recette",
                column: "Recette_id");

            migrationBuilder.CreateIndex(
                name: "fk_Utilisateur_has_Ingredient_Ingredient1_idx",
                table: "ListCourse",
                column: "Ingredient_id");

            migrationBuilder.CreateIndex(
                name: "fk_Utilisateur_has_Ingredient_Utilisateur1_idx",
                table: "ListCourse",
                column: "Utilisateur_id");

            migrationBuilder.CreateIndex(
                name: "fk_Recette_Image_idx",
                table: "Recette",
                column: "Image_id");

            migrationBuilder.CreateIndex(
                name: "fk_Utilisateur_Image_idx",
                table: "Utilisateur",
                column: "Image_idImage");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Etape");

            migrationBuilder.DropTable(
                name: "Favori");

            migrationBuilder.DropTable(
                name: "Frigo");

            migrationBuilder.DropTable(
                name: "Ingredient_has_Recette");

            migrationBuilder.DropTable(
                name: "ListCourse");

            migrationBuilder.DropTable(
                name: "Recette");

            migrationBuilder.DropTable(
                name: "Ingredient");

            migrationBuilder.DropTable(
                name: "Utilisateur");

            migrationBuilder.DropTable(
                name: "Categorie");

            migrationBuilder.DropTable(
                name: "Image");
        }
    }
}
