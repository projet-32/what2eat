﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace API.Migrations
{
    public partial class Fraise : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Ingredient",
                columns: new[] { "idIngredient", "Categorie_id", "codeBarre", "contenant", "Image_id", "nom", "quantite", "unite" },
                values: new object[] { 13, 1, "", "Sachet", 1, "fraises", 500, "g" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Ingredient",
                keyColumn: "idIngredient",
                keyValue: 13);
        }
    }
}
