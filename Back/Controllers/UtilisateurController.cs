using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using API.Models.DTO;
using API.Utils;
using API.Data;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilisateurController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly ModelToDTO DTOUtils;
        private TokenUtils _tokenUtils;
        private readonly ILogger<UtilisateurController> _logger;
        public UtilisateurController(DataContext context, ILogger<UtilisateurController> logger)
        {
            _context = context;
            DTOUtils = new ModelToDTO(_context);
            _tokenUtils = new TokenUtils();
            _logger = logger;
            _logger.LogDebug("Hey, this is a DEBUG message.");
            _logger.LogInformation("Hey, this is an INFO message.");
            _logger.LogWarning("Hey, this is a WARNING message.");
            _logger.LogError("Hey, this is an ERROR message.");
        }
        //  POST: api/Utilisateur/login
        [HttpPost("Login")]
        public async Task<ActionResult<UtilisateurDTO>> Login(string prenom, string motdepasse)
        {
            var users = await _context.Utilisateurs.ToListAsync();
            var isLogged = false;
            var userLogged = new UtilisateurDTO();

            foreach (Utilisateur user in users)
            {
                if (user.Prenom.Equals(prenom) && user.MotDePasse.Equals(motdepasse))
                {
                    userLogged = await DTOUtils.UtilisateurToDTO(user, null);
                    isLogged = true;
                    break;
                }
            }
            if (isLogged)
            {

                // génération d'un nouveau token, sauvegarde en base et retour de l'user
                userLogged.Token = _tokenUtils.GenerateJwtToken(userLogged.Id);
                List<IngredientDTO> ingredients = await GetIngredients(userLogged.Id);
                userLogged.Frigo = ingredients;

                return userLogged;
            }
            else
            {
                return NotFound();
            }
        }
        

        // POST: api/Utilisateur
        [HttpPost]
        public async Task<ActionResult<UtilisateurDTO>> PostUtilisateur(String nom, String prenom, String motdepasse, String monsecret)
        {
            Utilisateur u = new Utilisateur();
            u.Nom = nom; u.Prenom = prenom; u.MotDePasse = motdepasse; u.MonSecret = monsecret; u.ImageId = 1;
            _context.Utilisateurs.Add(u);
            await _context.SaveChangesAsync();


            //return CreatedAtAction(nameof(Login), new { prenom = u.Prenom, motdepasse = u.MotDePasse });

            var users = await _context.Utilisateurs.ToListAsync();
            var isLogged = false;
            var userLogged = new UtilisateurDTO();

            foreach (Utilisateur user in users)
            {
                if (user.Prenom.Equals(prenom) && user.MotDePasse.Equals(motdepasse))
                {
                    userLogged = await DTOUtils.UtilisateurToDTO(user, null);
                    isLogged = true;
                    break;
                }
            }
            if (isLogged)
            {

                // génération d'un nouveau token, sauvegarde en base et retour de l'user
                userLogged.Token = _tokenUtils.GenerateJwtToken(userLogged.Id);
                List<IngredientDTO> ingredients = await GetIngredients(userLogged.Id);
                userLogged.Frigo = ingredients;

                return userLogged;
            }
            else
            {
                return NotFound();
            }
        }
        
        // GET: api/GetUsers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Utilisateur>>> GetUsers()
        {

            return await _context.Utilisateurs.ToListAsync();
        }

        // GET: api/Utilisateur/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UtilisateurDTO>> GetUtilisateur(int id, string token)
        {

            if (id == _tokenUtils.ValidateJwtToken(token))
            {
                var utilisateur = await _context.Utilisateurs.FindAsync(id);

                List<IngredientDTO> ingredients = await GetIngredients(id);

                if (utilisateur == null)
                {
                    return NotFound();
                }

                return await DTOUtils.UtilisateurToDTO(utilisateur, ingredients, token);
            }

            return Unauthorized();

        }

        // PUT: api/Utilisateur
        [HttpPut]
        public async Task<ActionResult<List<Utilisateur>>> UpdateUtilisateur(UtilisateurDTO utilisateur, string token)
        {
            if (utilisateur.Id == _tokenUtils.ValidateJwtToken(token))
            {
                    var dbutilisateur = await _context.Utilisateurs.FindAsync(utilisateur.Id);
                if (dbutilisateur == null)
                    return BadRequest("Utilisateur not found.");

                dbutilisateur.Nom = utilisateur.Nom;
                dbutilisateur.Prenom = utilisateur.Prenom;
                dbutilisateur.MotDePasse = utilisateur.MotDePasse;
                dbutilisateur.MonSecret = utilisateur.MonSecret;
                //dbutilisateur.Frigo = utilisateur.Frigo;
                await _context.SaveChangesAsync();
                return Ok(dbutilisateur);
            }
            return Unauthorized();
        }

        // DELETE: api/Utilisateur/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUtilisateur(int id, string token)
        {
            if (id == _tokenUtils.ValidateJwtToken(token))
            {
                var utilisateur = await _context.Utilisateurs.FindAsync(id);
                if (utilisateur == null)
                {
                    return NotFound();
                }

                _context.Utilisateurs.Remove(utilisateur);
                await _context.SaveChangesAsync();

                return NoContent();
            }

            return Unauthorized();
        }
        

        // Get: api/Utilisateur/ingredients
        [HttpGet("Utilisateur/ingredients")]
        public async Task<List<IngredientDTO>> GetIngredients(int userId)
        {
            var frigos = await _context.Frigos.ToListAsync();

            List<IngredientDTO> ingredients = new List<IngredientDTO>();

            foreach (var frigo in frigos)
            {
                if (userId.Equals(frigo.UtilisateurId))
                {
                    var ingredient = await _context.Ingredients.FindAsync(frigo.IngredientId);
                    var ingredientDTO = await DTOUtils.IngredientToDTOAsync(ingredient);
                    ingredients.Add(ingredientDTO);
                }
            }
            return ingredients;
        }

        private bool UtilisateurExists(int id)
        {
            return _context.Utilisateurs.Any(e => e.Id == id);
        }

    }
}