﻿using System.Collections.Generic;

namespace API.Models
{
    public class Image
    {
        public Image()
        {
            Ingredients = new HashSet<Ingredient>();
            Recettes = new HashSet<Recette>();
            Utilisateurs = new HashSet<Utilisateur>();
        }
        public int Id { get; set; }
        public string Image64 { get; set; }
        public string ImagePath { get; set; }

        // propriété de navigation
        public virtual ICollection<Ingredient> Ingredients { get; set; }
        public virtual ICollection<Recette> Recettes { get; set; }
        public virtual ICollection<Utilisateur> Utilisateurs { get; set; }
    }
}