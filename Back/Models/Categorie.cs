﻿using System.Collections.Generic;
namespace API.Models
{
    public class Categorie
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public virtual ICollection<Ingredient> Ingredients { get; set; }
    }
}