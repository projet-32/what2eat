﻿namespace API.Models
{
    public class IngredientHasRecette
    {
        public int IngredientId { get; set; }
        public int RecetteId { get; set; }

        public virtual Ingredient Ingredient { get; set; }
        public virtual Recette Recette { get; set; }
    }
}
