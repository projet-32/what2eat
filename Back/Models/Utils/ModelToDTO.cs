﻿using API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models.DTO;

namespace API.Utils
{
    public class ModelToDTO
    {
        private readonly DataContext _context;

        public ModelToDTO(DataContext context)
        {
            _context = context;
        }

        public async Task<UtilisateurDTO> UtilisateurToDTO(Utilisateur utilisateur, List<IngredientDTO> userIngredients)
        {
            var image = await _context.Images.FindAsync(utilisateur.ImageId);

            return new UtilisateurDTO
            {
                Id = utilisateur.Id,
                Nom = utilisateur.Nom,
                Prenom = utilisateur.Prenom,
                MonSecret = utilisateur.MonSecret,
                MotDePasse = utilisateur.MotDePasse,
                ImageUrl = image.ImagePath,
                Frigo = userIngredients
            };
        }

        public async Task<UtilisateurDTO> UtilisateurToDTO(Utilisateur utilisateur, List<IngredientDTO> userIngredients,string token)
        {
            var image = await _context.Images.FindAsync(utilisateur.ImageId);

            return new UtilisateurDTO
            {
                Id = utilisateur.Id,
                Nom = utilisateur.Nom,
                Prenom = utilisateur.Prenom,
                MonSecret = utilisateur.MonSecret,
                MotDePasse = utilisateur.MotDePasse,
                ImageUrl = image.ImagePath,
                Frigo = userIngredients,
                Token = token
            };
        }

        public async Task<IngredientDTO> IngredientToDTOAsync(Ingredient ingredient)
        {

            var image = await _context.Images.FindAsync(ingredient.ImageId);
            var categorie = await _context.Categories.FindAsync(ingredient.CategorieId);

            return new IngredientDTO
            {
                Id = ingredient.Id,
                Nom = ingredient.Nom,
                CodeBarre = ingredient.CodeBarre,
                Quantite = ingredient.Quantite,
                Unite = ingredient.Unite,
                ImageUrl = image.ImagePath,
                Categorie = categorie.Nom
            };
        }
    }
}