﻿using API.Models;
using System;
using System.Collections.Generic;
using API.Models.DTO;

namespace API.Utils
{
    public class DTOToModel
    {
        private readonly DataContext _context;

        public DTOToModel(DataContext context)
        {
            _context = context;
        }

        public Ingredient DTOToIngredient(IngredientDTO ingredientDTO)
        {
            // création d'une image en bdd à partir de l'image url
            var image = new Image
            {
                ImagePath = ingredientDTO.ImageUrl
            };

            var categorie = new Categorie
            {
                Nom = ingredientDTO.Categorie
            };

            _context.Images.Add(image);
            _context.Categories.Add(categorie);

            _context.SaveChanges();

            var ingredient = new Ingredient
            {
                Nom = ingredientDTO.Nom,
                CodeBarre = ingredientDTO.CodeBarre,
                Quantite = ingredientDTO.Quantite,
                Unite = ingredientDTO.Unite,

                ImageId = image.Id, //on récupère ensuite l'id de l'image créee (clé etrangère)
                CategorieId = categorie.Id
            };

            return ingredient;
        }

        public Recette DTOToRecette(RecetteDTO recetteDTO)
        {
            var image = new Image
            {
                ImagePath = recetteDTO.ImageUrl
            };

            _context.Images.Add(image);
            _context.SaveChanges();

            var recette = new Recette
            {
                Nom = recetteDTO.Nom,
                ImageId = image.Id,
                Temps = recetteDTO.Temps,
                Personne = recetteDTO.Personne
            };

            return recette;
        }
    }
}