﻿namespace API.Models
{
    public class Favori
    {
        public int UtilisateurId { get; set; }
        public int RecetteId { get; set; }
        public virtual Recette Recette { get; set; }
        public virtual Utilisateur Utilisateur { get; set; }
    }
}
