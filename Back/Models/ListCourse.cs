﻿namespace API.Models
{
    public class ListCourse
    {
        public int UtilisateurId { get; set; }
        public int IngredientId { get; set; }

        public virtual Ingredient Ingredient { get; set; }
        public virtual Utilisateur Utilisateur{ get; set; }
    }
}
