﻿using System.Collections.Generic;
namespace API.Models
{
    public class Recette
    {
        public Recette()
        {
            Favoris = new HashSet<Favori>();
            Etapes = new HashSet<Etape>();
            IngredientHasRecettes = new HashSet<IngredientHasRecette>();
        }
        public int Id { get; set; }
        public string Nom { get; set; }
        public int? Temps { get; set; }
        public int? Personne { get; set; }

        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }

        public virtual ICollection<Favori> Favoris { get; set; }
        public virtual ICollection<Etape> Etapes { get; set; }
        public virtual ICollection<IngredientHasRecette> IngredientHasRecettes { get; set; }
    }
}
