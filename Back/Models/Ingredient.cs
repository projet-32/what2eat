﻿using System.Collections.Generic;
namespace API.Models
{
    public class Ingredient
    {
        public Ingredient()
        {
            Frigos = new HashSet<Frigo>();
            IngredientHasRecettes = new HashSet<IngredientHasRecette>();
            ListCourses = new HashSet<ListCourse>();
        }
        public int Id { get; set; }
        public string Nom { get; set; }
        public string CodeBarre { get; set; }
        public int Quantite { get; set; }
        public string Contenant { get; set; }
        public string Unite { get; set; }

        // Foreign Key
        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }
        public int? CategorieId { get; set; }
        public virtual Categorie Categorie { get; set; }
        public virtual ICollection<Frigo> Frigos { get; set; }
        public virtual ICollection<IngredientHasRecette> IngredientHasRecettes { get; set; }
        public virtual ICollection<ListCourse> ListCourses { get; set; }
    }
}
