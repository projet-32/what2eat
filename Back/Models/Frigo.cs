﻿namespace API.Models
{
    public class Frigo
    {
        public int UtilisateurId { get; set; }
        public virtual Utilisateur Utilisateur { get; set; }
        public int IngredientId { get; set; }
        public virtual Ingredient Ingredient { get; set; }

    }
}
