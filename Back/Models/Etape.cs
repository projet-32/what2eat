﻿namespace API.Models
{
    public class Etape
    {

        public int Id { get; set; }
        public string Description { get; set; }
        public int Position { get; set; }

        // Foreign Key
        public int? RecetteId { get; set; }
        public virtual Recette Recette { get; set; }


    }
}
