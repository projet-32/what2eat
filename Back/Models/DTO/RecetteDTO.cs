﻿using System;
namespace API.Models.DTO
{
    public class RecetteDTO
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string ImageUrl { get; set; }
        public int? Temps { get; set; }
        public int? Personne { get; set; }
    }
}
