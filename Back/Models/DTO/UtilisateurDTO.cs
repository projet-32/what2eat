﻿using System;
using System.Collections.Generic;

namespace API.Models.DTO
{
    public class UtilisateurDTO
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string MotDePasse { get; set; }
        public string MonSecret { get; set; }
        public string ImageUrl { get; set; }
        public string Token { get; set; }
        public List<IngredientDTO> Frigo { get; set; }
        public List<IngredientDTO> ListeCourse { get; set; }
        public List<RecetteDTO> Favoris { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}