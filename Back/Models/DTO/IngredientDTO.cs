﻿using System;
namespace API.Models.DTO
{
    public class IngredientDTO
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string CodeBarre { get; set; }
        public int Quantite { get; set; }
        public string Contenant { get; set; }
        public string Unite { get; set; }
        public string ImageUrl { get; set; }
        public string Categorie { get; set; }
        public IngredientDTO() { }

    }
    

}