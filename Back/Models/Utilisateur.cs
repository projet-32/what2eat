﻿using System.Collections.Generic;
namespace API.Models
{
    public class Utilisateur
    {
        public Utilisateur()
        {
            Favoris = new HashSet<Favori>();
            Frigos = new HashSet<Frigo>();
            ListCourses = new HashSet<ListCourse>();
        }
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string MonSecret { get; set; }
        
        public string MotDePasse { get; set; }

        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }

        public virtual ICollection<Favori> Favoris { get; set; }
        public virtual ICollection<Frigo> Frigos { get; set; }
        public virtual ICollection<ListCourse> ListCourses { get; set; }
    }
}
