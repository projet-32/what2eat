﻿using API.Models;
using API.Models.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace API.Service
{
    public class photoService
    {
        public static string Base64StringEncode(string str)
        {
            if (!String.IsNullOrEmpty(str))
                return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
            return null;
        }
        public static string Base64ByteEncode(byte[] str)
        {
            if (str != null)
                return Convert.ToBase64String(str);
            return null;
        }
        public static string Base64StringDecode(string str)
        {
            if (str.Length % 4 == 2)
            {
                str += "==";
            }
            else if (str.Length % 4 == 3)
            {
                str += "==";
            }
            Console.WriteLine(str);
            if (!String.IsNullOrEmpty(str))
                return Encoding.UTF8.GetString(Convert.FromBase64String(str));
            return null;
        }

        public static async Task<IngredientDTO> getImageOpenFoodApi(string str)
        {
            //ProductOpenFood obj;
            // notre cible
            string page = "https://world.openfoodfacts.org/api/v0/product/"+str+".json";

            using (HttpClient client = new HttpClient())
            {
                using (var http = new HttpClient())
                {
                    var result = http.GetAsync(page).Result;
                    var json = result.Content.ReadAsStringAsync().Result;
                    IngredientDTO ing = new IngredientDTO(); 
                    Console.WriteLine(json);

                    var myJObject = JObject.Parse(json);
                    var code = myJObject.SelectToken("code").Value<string>();
                    var nom = myJObject.SelectToken("product").SelectToken("product_name").Value<string>();

                    ing.Nom = nom.ToString();
                    ing.CodeBarre = code.ToString();
                    return ing;
                }
            }
            return null;
        }
    }
}
