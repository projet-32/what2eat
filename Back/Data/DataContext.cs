﻿using API.Models;

namespace API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        /*
         * Un DbSet<TEntity> peut être utilisé pour interroger et enregistrer des instances de TEntity .
         */
        public DbSet<Utilisateur> Utilisateurs { get; set; }
        public DbSet<Categorie> Categories { get; set; }
        public DbSet<Frigo> Frigos { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Etape> Etapes { get; set; }
        public DbSet<Favori> Favoris { get; set; }
        public DbSet<IngredientHasRecette> IngredientHasRecettes { get; set; }
        public DbSet<ListCourse> ListCourses { get; set; }
        public DbSet<Recette> Recettes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categorie>(entity =>
            {
                entity.ToTable("Categorie");
                entity.HasKey(p => p.Id);
                entity.Property(p => p.Id).HasColumnType("int").HasColumnName("id");
                entity.Property(p => p.Nom).HasMaxLength(255).HasColumnName("nom");
            });
            modelBuilder.Entity<Frigo>(entity =>
            {
                entity.ToTable("Frigo");
                entity.HasKey(p => new { p.IngredientId, p.UtilisateurId });

                entity.HasIndex(e => e.IngredientId, "fk_Utilisateur_has_Ingredient_Ingredient_idx");
                entity.HasIndex(e => e.UtilisateurId, "fk_Utilisateur_has_Ingredient_Utilisateur_idx");

                entity.Property(e => e.UtilisateurId).HasColumnType("int").HasColumnName("Utilisateur_id");
                entity.Property(e => e.IngredientId).HasColumnType("int").HasColumnName("Ingredient_id");

                // Relation ManytoMany
                entity.HasOne(p => p.Ingredient)
                    .WithMany(q => q.Frigos)
                    .HasForeignKey(r => r.IngredientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Utilisateur_has_Ingredient_Ingredient");
                entity.HasOne(d => d.Utilisateur)
                    .WithMany(p => p.Frigos)
                    .HasForeignKey(d => d.UtilisateurId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Utilisateur_has_Ingredient_Utilisateur");
            });

            modelBuilder.Entity<Image>(entity =>
            {
                entity.ToTable("Image");
                entity.HasKey(p => p.Id);
                entity.Property(p => p.Id).HasColumnType("int").HasColumnName("idImage");
                entity.Property(p => p.Image64).HasColumnName("image_64");
                entity.Property(p => p.ImagePath).HasMaxLength(255).HasColumnName("image_path");
            });

            modelBuilder.Entity<Ingredient>(entity =>
            {
                entity.ToTable("Ingredient");
                entity.HasKey(p => p.Id);

                entity.HasIndex(p => p.CategorieId, "fk_Ingredient_Categorie_idx");
                entity.HasIndex(p => p.ImageId, "fk_Ingredient_Image_idx");

                entity.Property(p => p.Id).HasColumnType("int").HasColumnName("idIngredient");
                entity.Property(p => p.CategorieId).HasColumnType("int").HasColumnName("Categorie_id");
                entity.Property(p => p.CodeBarre).HasMaxLength(20).HasColumnName("codeBarre");
                entity.Property(p => p.ImageId).HasColumnType("int").HasColumnName("Image_id");
                entity.Property(p => p.Nom).HasMaxLength(255).HasColumnName("nom");
                entity.Property(p => p.Quantite).HasColumnType("int").HasColumnName("quantite");
                entity.Property(p => p.Contenant).HasMaxLength(25).HasColumnName("contenant");
                entity.Property(p => p.Unite).HasMaxLength(255).HasColumnName("unite");

                entity.HasOne(p => p.Categorie)
                    .WithMany(q => q.Ingredients)
                    .HasForeignKey(r => r.CategorieId)
                    .HasConstraintName("fk_Ingredient_Categorie");

                entity.HasOne(p => p.Image)
                    .WithMany(q => q.Ingredients)
                    .HasForeignKey(r => r.ImageId)
                    .HasConstraintName("fk_Ingredient_Image");
            });
            modelBuilder.Entity<Utilisateur>(entity =>
            {
                entity.ToTable("Utilisateur");  //Nom de la table
                entity.HasKey(p => p.Id);       //Id clée primaire

                entity.HasIndex(p => p.ImageId, "fk_Utilisateur_Image_idx");

                entity.Property(p => p.Id).HasColumnType("int").HasColumnName("idUtilisateur");
                entity.Property(p => p.ImageId).HasColumnType("int").HasColumnName("Image_idImage");
                entity.Property(p => p.MonSecret).HasMaxLength(255).HasColumnName("mon_secret");
                entity.Property(p => p.MotDePasse).HasMaxLength(255).HasColumnName("mot_de_passe");
                entity.Property(p => p.Nom).HasMaxLength(255).HasColumnName("nom");
                entity.Property(p => p.Prenom).HasMaxLength(255).HasColumnName("prenom");
                // Relation OnetoOne avec Image
                entity.HasOne(p => p.Image)
                    .WithMany(q => q.Utilisateurs)
                    .HasForeignKey(r => r.ImageId)
                    .HasConstraintName("fk_Utilisateur_Image");
            });
            modelBuilder.Entity<Etape>(entity =>
            {
                entity.ToTable("Etape");
                entity.HasKey(p => p.Id);
                entity.HasIndex(p => p.RecetteId, "fk_Etape_has_Recette_idx");

                entity.Property(p => p.Id).HasColumnType("int").HasColumnName("idEtape");
                entity.Property(p => p.Description).HasMaxLength(255).HasColumnName("description");
                entity.Property(p => p.Position).HasColumnType("int").HasColumnName("position");
                entity.Property(p => p.RecetteId).HasColumnType("int").HasColumnName("Recette_id");

                entity.HasOne(p => p.Recette)
                    .WithMany(q => q.Etapes)
                    .HasForeignKey(r => r.RecetteId)
                    .HasConstraintName("fk_Etape_has_Recette");
            });
            modelBuilder.Entity<Favori>(entity =>
            {
                entity.ToTable("Favori");
                entity.HasKey(p => new { p.UtilisateurId, p.RecetteId });
                
                entity.HasIndex(p => p.RecetteId, "fk_Utilisateur_has_Recette_Recette_idx");
                entity.HasIndex(p => p.UtilisateurId, "fk_Utilisateur_has_Recette_Utilisateur_idx");

                entity.Property(p => p.UtilisateurId).HasColumnType("int").HasColumnName("Utilisateur_id");
                entity.Property(p => p.RecetteId).HasColumnType("int").HasColumnName("Recette_id");

                entity.HasOne(p => p.Recette)
                    .WithMany(q => q.Favoris)
                    .HasForeignKey(r => r.RecetteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Utilisateur_has_Recette_Recette");

                entity.HasOne(p => p.Utilisateur)
                    .WithMany(q => q.Favoris)
                    .HasForeignKey(r => r.UtilisateurId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Utilisateur_has_Recette_Utilisateur");
            });
            modelBuilder.Entity<IngredientHasRecette>(entity =>
            {
                entity.ToTable("Ingredient_has_Recette");
                entity.HasKey(p => new {p.IngredientId, p.RecetteId });

                entity.HasIndex(p => p.IngredientId, "fk_Ingredient_has_Recette_Ingredient_idx");
                entity.HasIndex(p => p.RecetteId, "fk_Ingredient_has_Recette_Recette_idx");

                entity.Property(p => p.IngredientId).HasColumnType("int").HasColumnName("Ingredient_id");
                entity.Property(p => p.RecetteId).HasColumnType("int").HasColumnName("Recette_id");

                entity.HasOne(p => p.Ingredient)
                    .WithMany(q => q.IngredientHasRecettes)
                    .HasForeignKey(r => r.IngredientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Ingredient_has_Recette_Ingredient");

                entity.HasOne(p => p.Recette)
                    .WithMany(q => q.IngredientHasRecettes)
                    .HasForeignKey(r => r.RecetteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Ingredient_has_Recette_Recette");
            });
            modelBuilder.Entity<ListCourse>(entity =>
            {
                entity.ToTable("ListCourse");
                entity.HasKey(p => new { p.UtilisateurId, p.IngredientId});
   
                entity.HasIndex(p => p.IngredientId, "fk_Utilisateur_has_Ingredient_Ingredient1_idx");
                entity.HasIndex(p => p.UtilisateurId, "fk_Utilisateur_has_Ingredient_Utilisateur1_idx");

                entity.Property(p => p.UtilisateurId).HasColumnType("int").HasColumnName("Utilisateur_id");
                entity.Property(p => p.IngredientId).HasColumnType("int").HasColumnName("Ingredient_id");

                entity.HasOne(p => p.Ingredient)
                    .WithMany(q => q.ListCourses)
                    .HasForeignKey(r => r.IngredientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Utilisateur_has_Ingredient_Ingredient1");

                entity.HasOne(p => p.Utilisateur)
                    .WithMany(q => q.ListCourses)
                    .HasForeignKey(r => r.UtilisateurId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Utilisateur_has_Ingredient_Utilisateur1");
            });
            modelBuilder.Entity<Recette>(entity =>
            {
                entity.ToTable("Recette");
                entity.HasKey(p => p.Id);

                entity.HasIndex(p => p.ImageId, "fk_Recette_Image_idx");

                entity.Property(p => p.Id).HasColumnType("int").HasColumnName("idRecette");
                entity.Property(p => p.ImageId).HasColumnType("int").HasColumnName("Image_id");
                entity.Property(p => p.Nom).HasMaxLength(255).HasColumnName("nom");
                entity.Property(p => p.Personne).HasColumnType("int").HasColumnName("personne");
                entity.Property(p => p.Temps).HasColumnType("int").HasColumnName("temps");

                entity.HasOne(p => p.Image)
                    .WithMany(q => q.Recettes)
                    .HasForeignKey(r => r.ImageId)
                    .HasConstraintName("fk_Recette_Image");
            });

            modelBuilder.Entity<Categorie>().HasData(
                new Categorie { Id = 1, Nom = "Fruits" },
                new Categorie { Id = 2, Nom = "Conserves" },
                new Categorie { Id = 3, Nom = "Sodas" },
                new Categorie { Id = 4, Nom = "Legumes" },
                new Categorie { Id = 5, Nom = "Viandes" }
                );
            modelBuilder.Entity<Image>().HasData(
                new Image { Id = 1, Image64 = "", ImagePath = "/Photos/paysage.jpg" },
                new Image { Id = 2, Image64 = "", ImagePath = "/Photos/groseille.jpg"},
                new Image { Id = 3, Image64 = "", ImagePath = "/Photos/pamplemousse.jpg" },
                new Image { Id = 4, Image64 = "", ImagePath = "/Photos/papaye.jpg" },
                new Image { Id = 5, Image64 = "", ImagePath = "/Photos/pasteque.jpg" }
                );
            // Ajout Utlisateur
            modelBuilder.Entity<Utilisateur>().HasData(
                 new Utilisateur { Id = 1, Prenom = "antoine", Nom = "antoine", ImageId = 1, MotDePasse = "antoine", MonSecret = "antoine" },
                new Utilisateur { Id = 2, Prenom = "Carson", Nom = "Alexander", ImageId = 1, MotDePasse = "azerty", MonSecret = "secret" },
                new Utilisateur { Id = 3, Prenom = "Meredith", Nom = "Alonso", ImageId = 1, MotDePasse = "azerty", MonSecret = "secret" },
                new Utilisateur { Id = 4, Prenom = "Arturo", Nom = "Anand", ImageId = 1, MotDePasse = "azerty", MonSecret = "secret" },
                new Utilisateur { Id = 5, Prenom = "Gytis", Nom = "Barzdukas", ImageId = 1, MotDePasse = "azerty", MonSecret = "secret" },
                new Utilisateur { Id = 6, Prenom = "Yan", Nom = "Li", ImageId = 1, MotDePasse = "azerty", MonSecret = "secret" },
                new Utilisateur { Id = 7, Prenom = "Peggy", Nom = "Justice", ImageId = 1, MotDePasse = "azerty", MonSecret = "secret" },
                new Utilisateur { Id = 8, Prenom = "Laura", Nom = "Norman", ImageId = 1, MotDePasse = "azerty", MonSecret = "secret" },
                new Utilisateur { Id = 9, Prenom = "Nino", Nom = "Olivetto", ImageId = 1, MotDePasse = "azerty", MonSecret = "secret" }
                );
            modelBuilder.Entity<Ingredient>().HasData(
                new Ingredient { Id = 1, Contenant="Sachet", CategorieId = 2, Nom = "Tranche de Pain Hamburger", Quantite = 4, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 2, Contenant = "Sachet", CategorieId = 3, Nom = "Salade", Quantite = 4, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 3, Contenant = "Sachet", CategorieId = 4, Nom = "Tomates", Quantite = 4, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 4, Contenant = "Sachet", CategorieId = 5, Nom = "Steak", Quantite = 4, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 5, Contenant = "Sachet", CategorieId = 1, Nom = "Groseille", Quantite = 7, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 6, Contenant = "Sachet", CategorieId = 1, Nom = "Pamplemousse", Quantite = 14, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 7, Contenant = "Sachet", CategorieId = 1, Nom = "Papaye", Quantite = 4, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 8, Contenant = "Sachet", CategorieId = 1, Nom = "Pastèque", Quantite = 9, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 9, Contenant = "Sachet", CategorieId = 1, Nom = "Raisin", Quantite = 4, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 10, Contenant = "Bouteille", CategorieId = 3, Nom = "Coca Cola", Quantite = 7, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 11, Contenant = "Bouteille", CategorieId = 3, Nom = "Pepsi", Quantite = 4, Unite = "1", CodeBarre = "", ImageId = 1 },
                new Ingredient { Id = 12, Contenant = "Bouteille", CategorieId = 3, Nom = "S.Pellegrino", Quantite = 4, Unite = "1", CodeBarre = "", ImageId = 1 }

                );
            modelBuilder.Entity<Frigo>().HasData(
                new Frigo { IngredientId = 1, UtilisateurId = 1},
                new Frigo { IngredientId = 2, UtilisateurId = 1 },
                new Frigo { IngredientId = 3, UtilisateurId = 1 },
                new Frigo { IngredientId = 4, UtilisateurId = 1 },
                new Frigo { IngredientId = 5, UtilisateurId = 1 },
                new Frigo { IngredientId = 6, UtilisateurId = 1 },
                new Frigo { IngredientId = 7, UtilisateurId = 1 },
                new Frigo { IngredientId = 8, UtilisateurId = 1 },
                new Frigo { IngredientId = 9, UtilisateurId = 1 },
                new Frigo { IngredientId = 10, UtilisateurId = 1 },
                new Frigo { IngredientId = 11, UtilisateurId = 1 },
                new Frigo { IngredientId = 12, UtilisateurId = 1 }
                );

            modelBuilder.Entity<IngredientHasRecette>().HasData(
                new IngredientHasRecette { IngredientId = 1, RecetteId = 1 },
                new IngredientHasRecette { IngredientId = 2, RecetteId = 1 },
                new IngredientHasRecette { IngredientId = 3, RecetteId = 1 },
                new IngredientHasRecette { IngredientId = 4, RecetteId = 1 }
                );
            modelBuilder.Entity<Recette>().HasData(
                new Recette { Id = 1, Nom = "Hamburgers" , Temps = 15, Personne=1, ImageId = 1}
                );
            modelBuilder.Entity<Etape>().HasData(
                new Etape { Id = 1, Description = "Poser dans une assiette une tranche de pain", Position = 1, RecetteId = 1 },
                new Etape { Id = 2, Description = "Poser ensuite une feuille de salade", Position = 2, RecetteId = 1 },
                new Etape { Id = 3, Description = "Poser délicatement le skeak précuit", Position = 3, RecetteId = 1 },
                new Etape { Id = 4, Description = "Poser ensuite une rondelle de tomate", Position = 4, RecetteId = 1 },
                new Etape { Id = 5, Description = "Recouvrer le tout part une autre tranche de pain", Position = 5, RecetteId = 1 },
                new Etape { Id = 6, Description = "Mettre au four 10 min", Position = 6, RecetteId = 1 }
                ) ;

            modelBuilder.Entity<Ingredient>().HasData(
               new Ingredient { Id = 13, Contenant = "Sachet", CategorieId = 1, Nom = "fraises", Quantite = 500, Unite = "g", CodeBarre = "", ImageId = 1 }
               );
        }
    }
}
