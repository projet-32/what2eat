# [Readme Principal](../Readme.md)
# Change Connection String

A changer pour chaque environement

Startup.cs

services.AddDbContext<DataContext>

# Run bdd Docker
!important ce mettre en bash

docker pull mcr.microsoft.com/mssql/server:2022-latest

## Demarrage de sql server
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=changeit_CHANGEIT" -p 1433:1433 --name sqlWHat2Eat -h sqlWHat2Eat -d mcr.microsoft.com/mssql/server:2022-latest

## Redifinition du mot de passe alpha_PWD 
docker exec -it sqlWHat2Eat /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "changeit_CHANGEIT" -Q "ALTER LOGIN SA WITH PASSWORD=\"alpha_PWD\""

## Connexion

docker exec -it sqlWHat2Eat "bash"

/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "alpha_PWD"

SELECT  * FROM  SYSOBJECTS WHERE  xtype = 'U';
Go
SELECT   * FROM   information_schema.tables WHERE   table_type='BASE TABLE';
Go

# Run bdd Azure
## Commande
### Création de la base avec un import .sql
.\azure.sh

### Suprime la resource
.\EndAzure.sh