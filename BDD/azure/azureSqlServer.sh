#az account set -s $subscription # ...or use 'az login'
az login
# Création de la base de données
location="East US"
randomIdentifier="what2eat"
resource="$randomIdentifier-resource"
sqlserver="$randomIdentifier-sqlserver" ## modifier sqlserver
database="$randomIdentifier-database"
login="antoine"
password="alpha_PWD"
startIP=0.0.0.0
endIP=255.0.0.0

echo -e "Utilisation resource group $resource avec login: $login, password: $password "
az group create --name $resource --location "$location"

echo -e "Creation $sqlserver Azure Database  on $resource "
az sql server create --name $sqlserver --resource-group $resource --location "$location" --admin-user $login --admin-password $password

echo -e "Configuration firewall "
az sql server firewall-rule create --resource-group $resource --server $sqlserver --name AllowMyIP --start-ip-address $startIP --end-ip-address $endIP
exit 1

# Create user
CREATE LOGIN thibaut WITH PASSWORD = 'beta_PWD'
GO

CREATE USER thibaut
	FOR LOGIN thibaut
	WITH DEFAULT_SCHEMA = db_datawriter
GO

USE [what2eat]
GO
ALTER AUTHORIZATION ON SCHEMA::[db_datawriter] TO [thibaut]
GO

# Update database
# dotnet ef database update

#echo -e "\E[32m SHOW $database on $server \E[0m"
#az mysql server show --resource-group $resource --name $server
#
#echo -e "\E[32m create shéma in $database on $server \E[0m"
#mysql -h what2eat-server.mysql.database.azure.com -u antoine@what2eat-server -p < createAzureDatabase.sql
#
#echo -e "\E[32m import data in $database on $server \E[0m"
#mysql -h what2eat-server.mysql.database.azure.com -u antoine@what2eat-server -p < data.sql
