# Run
#!important ce mettre en bash
bash
docker pull mcr.microsoft.com/mssql/server:2022-latest

# Demarrage de sql server
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=changeit_CHANGEIT" -p 1433:1433 --name sqlWHat2Eat -h sqlWHat2Eat -d mcr.microsoft.com/mssql/server:2022-latest

# Redifinition du mot de passe alpha_PWD 
docker exec -it sqlWHat2Eat /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "changeit_CHANGEIT" -Q "ALTER LOGIN SA WITH PASSWORD=\"alpha_PWD\""