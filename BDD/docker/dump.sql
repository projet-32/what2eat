-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: food
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Categorie`
--

DROP TABLE IF EXISTS `Categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Categorie` (
  `idCategorie` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categorie`
--

LOCK TABLES `Categorie` WRITE;
/*!40000 ALTER TABLE `Categorie` DISABLE KEYS */;
INSERT INTO `Categorie` VALUES (1,'legume'),(2,'fruit');
/*!40000 ALTER TABLE `Categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Etape`
--

DROP TABLE IF EXISTS `Etape`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Etape` (
  `idEtape` int NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `position` int DEFAULT NULL,
  `Recette_idRecette` int DEFAULT NULL,
  PRIMARY KEY (`idEtape`),
  KEY `fk_Etape_Recette1_idx` (`Recette_idRecette`),
  CONSTRAINT `fk_Etape_Recette1` FOREIGN KEY (`Recette_idRecette`) REFERENCES `Recette` (`idRecette`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Etape`
--

LOCK TABLES `Etape` WRITE;
/*!40000 ALTER TABLE `Etape` DISABLE KEYS */;
INSERT INTO `Etape` VALUES (1,'Mélanger la farine, l\'huile et les oeufs.',1,1),(2,'Ajoutez ensuite le lait, la bière et le rhum.',2,1),(3,'Mélangez le tout : c\'est prêt en deux minutes avec un mixeur !',3,1),(4,'Faites cuire à la poêle avec une noix de beurre.',4,1),(5,'Verser la semoule dans un grand récipient et la noyer avec l\'eau. Remuer doucement.',2,2),(6,'Égoutter la semoule quand l\'eau devient blanche',3,2),(7,'Laisser reposer 15-20 mn (30 maxi), saler (1 cuillère à soupe rase), huiler (1 cuillère à soupe).',4,2),(8,'Malaxer pour briser les mottes.',5,2),(9,'Cuisson de la semoule :',6,2),(10,'Dans une couscoussière de 5 litres, remplir à moitié d\'eau la partie basse et verser la semoule dans la partie haute (ne pas mettre de couvercle).',7,2),(11,'Rendre hermétique si possible la couscoussière au niveau de la jonction des 2 parties afin d\'éviter l\'évaporation de la vapeur.',8,2),(12,'Compter une cuisson de 20 min à partir de l\'apparition de la vapeur à travers la semoule.',9,2),(13,'Verser la semoule dans un récipient, remuer doucement avec une cuillère en bois pour briser les mottes, ajouter 50 g de beurre et mélanger doucement.',10,2),(14,'Légumes et viandes :',11,2),(15,'Mettre dans une grande casserole 3 c à soupe d\'huile, faire revenir les 2 oignons, les 2/3 gousses d\'ail et faire dorer la viande. Saler, poivrer, ajouter les carottes en rondelles. Laisser cuire à feu doux 15 min.',12,2),(16,'Ajouter le concentré de tomate puis 2 litres d\'eau bouillante.',13,2),(17,'Laisser cuire 20 min.',14,2),(18,'Pendant cet temps, cuire à part dans l\'eau salée les navets en morceaux pendant 20 min. Ajouter 5 min avant la fin les courgettes en rondelles. Égoutter en fin de cuisson.',15,2),(19,'Quand la viande est cuite, y ajouter les navets et courgettes et les pois chiches.',16,2),(20,'Laisser cuire le tout 5 à 10 min.',17,2),(21,'Retirer 1 louche du jus de cuisson de la viande, ajouter 1 cuillère à café de harissa. La servir à part.',18,2),(22,'Coupez les 3 fromages en petits dés.',1,3),(23,'Frottez le caquelon avec la gousse d\'ail et laissez-la dedans.',2,3),(24,'Versez 25 cl de vin blanc et faites chauffer.',3,3),(25,'Dans un petit récipient, versez le vin blanc restant (5 cl), la Maïzena et la noix de muscade. Remuez et réservez.',4,3),(26,'Lorsque le vin \'frétille\', versez le fromage (en plusieurs fois) sans cesser de remuer (avec une spatule en bois) sur feu doux/moyen.',5,3),(27,'Lorsque le fromage est bien fondu, versez le verre vin/Maïzena/muscade et continuez de remuer doucement.',6,3),(28,'La préparation commence à être onctueuse et mousseuse, toujours un peu liquide. Poivrez et versez le kirsch. C\'est prêt à servir!',7,3),(29,'Le jaune d\'oeuf est à mettre dans le caquelon lorsqu\'il ne reste plus de liquide, afin d erécupérer le reste du fromage au fond.',8,3),(30,'Eplucher et hacher les oignons. Eplucher et hacher les gousses d\'ail.',1,4),(31,'Mettre la moitié des oignons dans la chair à saucisse. Ajouter l\'ail, le sel, le poivre et un peu de persil.',2,4),(32,'Couper le haut des tomates et les évider. Poivrer et saler l\'intérieur. Mettre la farce à l\'intérieur et remettre les chapeaux.',3,4),(33,'Mettre le reste des oignons dans un plat avec la chair des tomates.',4,4),(34,'Mettre les tomates farcies dans le plat. Parsemez d\'un peu de thym et mette une noisette de beurre sur chaque tomates. Faire cuire au four chaud à 180°C (thermostat 6) pendant 1 heure environ.',5,4),(35,'Servir avec du riz.',6,4),(36,'Cuire les pâtes dans un grand volume d\'eau bouillante salée.',1,5),(37,'Emincer les oignons et les faire revenir à la poêle. Dès qu\'ils ont bien dorés, y ajouter les lardons.',2,5),(38,'Préparer dans un saladier la crème fraîche, les oeufs, le sel, le poivre et mélanger.',3,5),(39,'Retirer les lardons du feu dès qu\'ils sont dorés et les ajouter à la crème.',4,5),(40,'Une fois les pâtes cuite al dente, les égoutter et y incorporer la crème. Remettre sur le feu si le plat a refroidi.',5,5),(41,'Servir et bon appétit !\nVous pouvez également agrémenter votre plat avec des champignons.',6,5);
/*!40000 ALTER TABLE `Etape` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Favori`
--

DROP TABLE IF EXISTS `Favori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Favori` (
  `idFavori` int NOT NULL AUTO_INCREMENT,
  `Utilisateur_idUtilisateur` int NOT NULL,
  `Recette_idRecette` int NOT NULL,
  PRIMARY KEY (`idFavori`,`Utilisateur_idUtilisateur`,`Recette_idRecette`),
  KEY `fk_Utilisateur_has_Recette_Recette1_idx` (`Recette_idRecette`),
  KEY `fk_Utilisateur_has_Recette_Utilisateur1_idx` (`Utilisateur_idUtilisateur`),
  CONSTRAINT `fk_Utilisateur_has_Recette_Recette1` FOREIGN KEY (`Recette_idRecette`) REFERENCES `Recette` (`idRecette`),
  CONSTRAINT `fk_Utilisateur_has_Recette_Utilisateur1` FOREIGN KEY (`Utilisateur_idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Favori`
--

LOCK TABLES `Favori` WRITE;
/*!40000 ALTER TABLE `Favori` DISABLE KEYS */;
INSERT INTO `Favori` VALUES (1,1,1),(2,1,2),(3,2,3);
/*!40000 ALTER TABLE `Favori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Frigo`
--

DROP TABLE IF EXISTS `Frigo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Frigo` (
  `Utilisateur_idUtilisateur` int NOT NULL,
  `Ingredient_idIngredient` int NOT NULL,
  PRIMARY KEY (`Utilisateur_idUtilisateur`,`Ingredient_idIngredient`),
  KEY `fk_Utilisateur_has_Ingredient_Ingredient2_idx` (`Ingredient_idIngredient`),
  KEY `fk_Utilisateur_has_Ingredient_Utilisateur2_idx` (`Utilisateur_idUtilisateur`),
  CONSTRAINT `fk_Utilisateur_has_Ingredient_Ingredient2` FOREIGN KEY (`Ingredient_idIngredient`) REFERENCES `Ingredient` (`idIngredient`),
  CONSTRAINT `fk_Utilisateur_has_Ingredient_Utilisateur2` FOREIGN KEY (`Utilisateur_idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Frigo`
--

LOCK TABLES `Frigo` WRITE;
/*!40000 ALTER TABLE `Frigo` DISABLE KEYS */;
INSERT INTO `Frigo` VALUES (1,1),(2,1),(1,2),(3,3);
/*!40000 ALTER TABLE `Frigo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Image`
--

DROP TABLE IF EXISTS `Image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Image` (
  `idImage` int NOT NULL AUTO_INCREMENT,
  `image_64` text,
  `image_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idImage`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Image`
--

LOCK TABLES `Image` WRITE;
/*!40000 ALTER TABLE `Image` DISABLE KEYS */;
INSERT INTO `Image` VALUES (1,'','https://img.freepik.com/vecteurs-libre/logo-nourriture-verte_7297-183.jpg?size=626&ext=jpg'),(2,NULL,'https://image.freepik.com/vecteurs-libre/profil-avatar-homme-icone-ronde_24640-14044.jpg'),(3,NULL,'https://image.freepik.com/vecteurs-libre/profil-avatar-homme-icone-ronde_24640-14044.jpg'),(4,NULL,'https://www.lesfruitsetlegumesfrais.com/_upload/cache/ressources/produits/tomate/tomate_-_copie_346_346_filled.jpg'),(5,NULL,'https://www.biendecheznous.be/sites/default/files/ps_image/istock_courgettes.jpg'),(6,NULL,'https://img-3.journaldesfemmes.fr/LFYLLU8JV163I7rcv9-y_SrLuTs=/910x607/smart/e2d84215f5c44c648c3ecdcbf0ac1ccc/ccmcms-jdf/10662418.jpg'),(7,NULL,'https://alsace.nouvellesgastronomiques.com/photo/art/grande/45432100-36693657.jpg'),(8,NULL,'https://fr.rc-cdn.community.thermomix.com/recipeimage/2nc1g3lr-77de7-564089-cfcd2-b67ljhi0/0d0d5043-f22b-4b08-8d9c-2578e0802b37/main/salade-verte.jpg'),(9,NULL,'https://www.academiedugout.fr/images/15721/1200-auto/fotolia_55631648_subscription_xl-copy.jpg'),(10,NULL,'https://www.lesfruitsetlegumesfrais.com/_upload/cache/ressources/produits/radis/radis_vignette2_346_346_filled.jpg'),(11,NULL,'https://www.aprifel.com/wp-content/uploads/2019/02/kiwi.jpg'),(12,NULL,'https://img-3.journaldesfemmes.fr/QR6Wke_TSyfNZsTQDdYdAFeQ9Wc=/1240x/smart/46dbda9272fb4f74bb89fda8c58f61b1/ccmcms-jdf/10659242.jpg'),(13,NULL,'https://produits.bienmanger.com/35111-0w600h600_Pommes_Story_France_Bio.jpg'),(14,NULL,'https://assets.afcdn.com/recipe/20171019/73291_w600.jpg'),(15,NULL,'https://assets.afcdn.com/recipe/20200828/113350_w157h157c1.webp'),(16,NULL,'https://assets.afcdn.com/recipe/20200831/113575_w157h157c1.webp'),(17,NULL,'https://assets.afcdn.com/recipe/20161130/59380_w157h157c1.webp'),(18,NULL,'https://assets.afcdn.com/recipe/20171012/73069_w157h157c1.webp'),(19,NULL,'https://assets.afcdn.com/recipe/20120828/10062_w157h157c1.webp');
/*!40000 ALTER TABLE `Image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ingredient`
--

DROP TABLE IF EXISTS `Ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Ingredient` (
  `idIngredient` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `codeBarre` varchar(20) DEFAULT NULL,
  `quantite` int DEFAULT NULL,
  `unite` varchar(255) DEFAULT NULL,
  `Image_idImage` int DEFAULT NULL,
  `Categorie_idCategorie` int DEFAULT NULL,
  PRIMARY KEY (`idIngredient`),
  KEY `fk_Ingredient_Image1_idx` (`Image_idImage`),
  KEY `fk_Ingredient_Categorie1_idx` (`Categorie_idCategorie`),
  CONSTRAINT `fk_Ingredient_Categorie1` FOREIGN KEY (`Categorie_idCategorie`) REFERENCES `Categorie` (`idCategorie`),
  CONSTRAINT `fk_Ingredient_Image1` FOREIGN KEY (`Image_idImage`) REFERENCES `Image` (`idImage`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ingredient`
--

LOCK TABLES `Ingredient` WRITE;
/*!40000 ALTER TABLE `Ingredient` DISABLE KEYS */;
INSERT INTO `Ingredient` VALUES (1,'Tomate','0000000',5,'g',1,1),(2,'Courgette','0000000',2,'g',1,1),(3,'Fraise','0000000',10,'g',1,1),(4,'Citron','0000000',1,'g',1,2),(5,'Salade','0000000',2,'g',1,1),(6,'Oignon','0000000',5,'g',1,1),(7,'Radis','0000000',9,'g',1,1),(8,'Kiwi','0000000',1,'g',1,2),(9,'Concombre','0000000',3,'g',1,1),(10,'Pomme','0000000',7,'g',1,2);
/*!40000 ALTER TABLE `Ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ingredient_has_Recette`
--

DROP TABLE IF EXISTS `Ingredient_has_Recette`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Ingredient_has_Recette` (
  `idIngredient_has_Recettecol` int NOT NULL AUTO_INCREMENT,
  `Ingredient_idIngredient` int NOT NULL,
  `Recette_idRecette` int NOT NULL,
  PRIMARY KEY (`idIngredient_has_Recettecol`,`Ingredient_idIngredient`,`Recette_idRecette`),
  KEY `fk_Ingredient_has_Recette_Recette1_idx` (`Recette_idRecette`),
  KEY `fk_Ingredient_has_Recette_Ingredient1_idx` (`Ingredient_idIngredient`),
  CONSTRAINT `fk_Ingredient_has_Recette_Ingredient1` FOREIGN KEY (`Ingredient_idIngredient`) REFERENCES `Ingredient` (`idIngredient`),
  CONSTRAINT `fk_Ingredient_has_Recette_Recette1` FOREIGN KEY (`Recette_idRecette`) REFERENCES `Recette` (`idRecette`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ingredient_has_Recette`
--

LOCK TABLES `Ingredient_has_Recette` WRITE;
/*!40000 ALTER TABLE `Ingredient_has_Recette` DISABLE KEYS */;
INSERT INTO `Ingredient_has_Recette` VALUES (1,1,1),(2,1,2),(3,2,2),(4,3,3),(5,3,3),(6,4,4),(7,5,5),(8,5,5);
/*!40000 ALTER TABLE `Ingredient_has_Recette` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ListCourse`
--

DROP TABLE IF EXISTS `ListCourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ListCourse` (
  `idListCourse` int NOT NULL AUTO_INCREMENT,
  `Utilisateur_idUtilisateur` int NOT NULL,
  `Ingredient_idIngredient` int NOT NULL,
  PRIMARY KEY (`idListCourse`,`Utilisateur_idUtilisateur`,`Ingredient_idIngredient`),
  KEY `fk_Utilisateur_has_Ingredient_Ingredient1_idx` (`Ingredient_idIngredient`),
  KEY `fk_Utilisateur_has_Ingredient_Utilisateur1_idx` (`Utilisateur_idUtilisateur`),
  CONSTRAINT `fk_Utilisateur_has_Ingredient_Ingredient1` FOREIGN KEY (`Ingredient_idIngredient`) REFERENCES `Ingredient` (`idIngredient`),
  CONSTRAINT `fk_Utilisateur_has_Ingredient_Utilisateur1` FOREIGN KEY (`Utilisateur_idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ListCourse`
--

LOCK TABLES `ListCourse` WRITE;
/*!40000 ALTER TABLE `ListCourse` DISABLE KEYS */;
/*!40000 ALTER TABLE `ListCourse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Recette`
--

DROP TABLE IF EXISTS `Recette`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Recette` (
  `idRecette` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `Image_idImage` int DEFAULT NULL,
  `temps` int DEFAULT NULL,
  `personne` int DEFAULT NULL,
  PRIMARY KEY (`idRecette`),
  KEY `fk_Recette_Image1_idx` (`Image_idImage`),
  CONSTRAINT `fk_Recette_Image1` FOREIGN KEY (`Image_idImage`) REFERENCES `Image` (`idImage`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Recette`
--

LOCK TABLES `Recette` WRITE;
/*!40000 ALTER TABLE `Recette` DISABLE KEYS */;
INSERT INTO `Recette` VALUES (1,'Crêpes',14,10,6),(2,'Coucous',16,60,6),(3,'Fondue Savoyarde',17,60,6),(4,'Tomates farcies',18,54,4),(5,'Pâtes à la carbonara',19,15,4);
/*!40000 ALTER TABLE `Recette` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Utilisateur`
--

DROP TABLE IF EXISTS `Utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Utilisateur` (
  `idUtilisateur` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `naissance` date DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `mot_de_passe` varchar(255) DEFAULT NULL,
  `Image_idImage` int DEFAULT NULL,
  PRIMARY KEY (`idUtilisateur`),
  KEY `fk_Utilisateur_Image1_idx` (`Image_idImage`),
  CONSTRAINT `fk_Utilisateur_Image1` FOREIGN KEY (`Image_idImage`) REFERENCES `Image` (`idImage`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Utilisateur`
--

LOCK TABLES `Utilisateur` WRITE;
/*!40000 ALTER TABLE `Utilisateur` DISABLE KEYS */;
INSERT INTO `Utilisateur` VALUES (1,'Emonds','Umberto',NULL,'umberto.emonds@social.aston-ecole.com','admin',1),(2,'Specque','Antoine',NULL,'antoine.specque@social.aston-ecole.com','admin',2),(3,'Lobry Duval','Jeremy',NULL,'jeremy.lobryduval@social.aston-ecole.com','admin',3);
/*!40000 ALTER TABLE `Utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-05 15:42:02
