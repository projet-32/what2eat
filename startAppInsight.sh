#az account set -s $subscription # ...or use 'az login'
az login
# Création de la base de données
location="westeurope"
randomIdentifier="what2eat"
resource="$randomIdentifier-resource"
apikey="$randomIdentifier-api-key"
appins="$randomIdentifier-app-insight"
az group create --name $resource --location $location

az monitor app-insights api-key create --api-key $apikey --read-properties ReadTelemetry --write-properties WriteAnnotations -g $resource --app $appins
