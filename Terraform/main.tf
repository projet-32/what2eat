provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "what2eat" {
  name     = "what2eat-resource-group"
  location = "West Europe"
}

resource "azurerm_static_site" "what2eat" {
  name                = "what2eat-static-site"
  location            = azurerm_resource_group.what2eat.location
  resource_group_name = azurerm_resource_group.what2eat.name
  repository_url      = "https://github.com/what2eat/repo"
  branch              = "main"
}
#
#resource "azurerm_sql_server" "what2eat" {
#  name                         = "what2eat-sql-server"
#  resource_group_name          = azurerm_resource_group.what2eat.name
#  location                     = azurerm_resource_group.what2eat.location
#  version                      = "12.0"
#  administrator_login          = "admin"
#  administrator_login_password = "password"
#}
#
#resource "azurerm_sql_database" "what2eat" {
#  name                = "what2eat-sql-database"
#  resource_group_name = azurerm_resource_group.what2eat.name
#  location            = azurerm_resource_group.what2eat.location
#  server_name         = azurerm_sql_server.what2eat.name
#  edition             = "Standard"
#  collation           = "SQL_Latin1_General_CP1_CI_AS"
#}