# Fournisseur Azure
provider "azurerm" {
  features {}
}

# Ressource de groupe
resource "azurerm_resource_group" "example" {
  name     = "example-resource-group"
  location = "West Europe"
}

# Serveur SQL
resource "azurerm_sql_server" "example" {
  name                         = "example-sql-server"
  resource_group_name          = azurerm_resource_group.example.name
  location                     = azurerm_resource_group.example.location
  version                      = "12.0"
  administrator_login          = "sqladmin"
  administrator_login_password = "P@ssw0rd123!"
}

# Image Docker dotnet core
resource "azurerm_container_registry" "example" {
  name                     = "example-container-registry"
  resource_group_name      = azurerm_resource_group.example.name
  location                 = azurerm_resource_group.example.location
  sku                      = "Basic"
  admin_enabled            = true
  georeplication_locations = ["West Europe"]
}

# Image Docker nginx:alpine
resource "azurerm_container_registry" "example_nginx" {
  name                     = "example-nginx-container-registry"
  resource_group_name      = azurerm_resource_group.example.name
  location                 = azurerm_resource_group.example.location
  sku                      = "Basic"
  admin_enabled            = true
  georeplication_locations = ["West Europe"]
}

# Sortie des informations
output "sql_server_connection_string" {
  value = azurerm_sql_server.example.connection_strings[0]
}

output "container_registry_username" {
  value = azurerm_container_registry.example.admin_username
}

output "container_registry_password" {
  value = azurerm_container_registry.example.admin_password
}

output "container_registry_nginx_username" {
  value = azurerm_container_registry.example_nginx.admin_username
}

output "container_registry_nginx_password" {
  value = azurerm_container_registry.example_nginx.admin_password
}
