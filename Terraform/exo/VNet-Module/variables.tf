variable "resource_group_name" {
  description = "Nom du groupe de ressources"
  type        = string
}

variable "location" {
  description = "Emplacement du VNET"
  type        = string
  default     = "westus2"
}

variable "vnet_name" {
  description = "Nom du vnet"
  type        = string
}

variable "vnet_cidr" {
  description = "Nom du vnet_cidr"
  type        = string
}
variable "subnet_name" {
  description = "Nom du subnet_name"
  type        = string
}
variable "subnet_cidr" {
  description = "Nom du subnet_cidr"
  type        = string
}