terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0,<4.0.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = "dbf62859-39f7-4fdc-bb53-e526725de55e"
  tenant_id       = "419583d9-10bf-4640-938d-efe83663b051"

}

resource "azurerm_resource_group" "main" {
  name     = "my-resource-group"
  location = "westus2"
}

module "vnet" {
  source              = "./VNet-Module"
  resource_group_name = "my-resource-group"
  location            = "westus2"
  vnet_name           = "my-vnet"
  vnet_cidr           = "10.0.0.0/16"
  subnet_name         = "my-subnet"
  subnet_cidr         = "10.0.0.0/24"
  depends_on = [ azurerm_resource_group.main ]
}

module "ubuntu_vm_1" {
  source                  = "./VM-Ubuntu-Module"
  resource_group_name     = "my-resource-group"
  location                = "westus2"
  vm_name                 = "my-ubuntu-vm-1"
  vm_size                 = "Standard_B1s"
  availability_zone       = "1"
  vnet_subnet_id          = module.vnet.subnet_id
  vm_public_ip_name       = "my-public-ip-1"
  vm_public_ip_allocation = "Dynamic"
  depends_on = [ azurerm_resource_group.main, module.vnet ]
  
}

module "ubuntu_vm_2" {
  source                  = "./VM-Ubuntu-Module"
  resource_group_name     = "my-resource-group"
  location                = "westus2"
  vm_name                 = "my-ubuntu-vm-2"
  vm_size                 = "Standard_B1s"
  availability_zone       = "2"
  vnet_subnet_id          = module.vnet.subnet_id
  vm_public_ip_name       = "my-public-ip-2"
  vm_public_ip_allocation = "Dynamic"
  depends_on = [ azurerm_resource_group.main, module.vnet ]
}

module "ubuntu_vm_3" {
  source                  = "./VM-Ubuntu-Module"
  resource_group_name     = "my-resource-group"
  location                = "westus2"
  vm_name                 = "my-ubuntu-vm-3"
  vm_size                 = "Standard_B1s"
  availability_zone       = "3"
  vnet_subnet_id          = module.vnet.subnet_id
  vm_public_ip_name       = "my-public-ip-3"
  vm_public_ip_allocation = "Dynamic"
  depends_on = [ azurerm_resource_group.main, module.vnet ]
}