resource "azurerm_public_ip" "module-ubuntu" {
  name                = var.vm_public_ip_name
  location            = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = var.vm_public_ip_allocation
#  sku                 = "Standard"
#  zones    = [var.availability_zone]
}


resource "azurerm_network_interface" "module-ubuntu" {
  name                = "${var.vm_name}-nic"
  location            = var.location
  resource_group_name = var.resource_group_name
  ip_configuration {
    name                          = "config"
    subnet_id                     = var.vnet_subnet_id
    private_ip_address_allocation = var.vm_public_ip_allocation
    public_ip_address_id          = azurerm_public_ip.module-ubuntu.id
  }
}

resource "azurerm_virtual_machine" "module-ubuntu" {
  name                  = var.vm_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  vm_size               = var.vm_size
#  zones    = [var.availability_zone]
  network_interface_ids = [azurerm_network_interface.module-ubuntu.id]

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.vm_name}-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
    disk_size_gb      = 30
  }

  os_profile {
    computer_name  = var.vm_name
    admin_username = "adminuser"
    admin_password = "Password1234!"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}


