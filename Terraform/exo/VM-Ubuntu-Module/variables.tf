variable "vm_name" {
  description = "Nom de la machine virtuelle Ubuntu"
  type        = string
}

variable "vm_size" {
  description = "Taille de la machine virtuelle"
  type        = string
  default     = "Standard_DS2_v2"
}

variable "resource_group_name" {
  description = "Nom du groupe de ressources"
  type        = string
}

variable "location" {
  description = "Emplacement de la machine virtuelle"
  type        = string
  default     = "westus2"
}

variable "availability_zone" {
  description = "Zone de disponibilité de la machine virtuelle"
  type        = string
  default     = "1"
}

variable "vm_public_ip_name" {
  description = "Name ip public de la machine virtuelle"
  type        = string
}

variable "vm_public_ip_allocation" {
  description = "Name of methode ip public de la machine virtuelle"
  type        = string
}

variable "vnet_subnet_id" {
  description = "vnet_subnet_id"
  type        = string
}