import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Image, Ingredient, Utilisateur, Recette }from './model';
import { environment } from '../../environments/environment';


import { map } from 'rxjs/operators';
//https://what2eat-webapp.azurewebsites.net/api
//https://localhost:7073/api
const API = {
    ROOT: "https://localhost:7073/api",

    get GET_ALL_Utilisateur() {
        return this.ROOT + "/Utilisateur"
    },
    get GET_Utilisateur_ID() {
        return this.ROOT + "/Utilisateur/"
    },
    get UPDATE_Utilisateur_ID() {
        return this.ROOT + "/Utilisateur/update/"
    },
    get ADD_Utilisateur() {
        return this.ROOT + "/Utilisateur/add"
    },
    get GET_IMG_Utilisateur(){
        return this.ROOT + "/Utilisateur/findimg/"
    },
    get UPDATE_IMG_Utilisateur_ID(){
        return this.ROOT + "/Utilisateur/updateimg/"
    },
    get DELETE_Utilisateur_ID() {
        return this.ROOT + "/Utilisateur/delete/"
    },
    get POST_LOGIN() {
        return this.ROOT + "/Utilisateur/Login" //envoie les données pour check login
    },
    get POST_FORGOTPASSWORD() {
        return this.ROOT + "/forgotpassword"
    },
    get FAKE_Utilisateur() {
        return this.ROOT + "/FakeUtilisateur"
    }

}

@Injectable({
    providedIn: 'root'
  })
  export class AccessAPIService {


    constructor(private http: HttpClient) { }
    LoginUser(user) : Observable<HttpResponse<Utilisateur>>{
        console.log(user);
        if(environment.mode_no_bdd){
            return this.findFakeUtilisateurByID();
        }
        return this.http.post<Utilisateur>(API.POST_LOGIN + "?prenom=" + user.prenom + "&motdepasse=" + user.password,null,{observe:'response'});    
    }

    GetForgetPass(mail){
        return this.http.post(API.POST_FORGOTPASSWORD,mail)
    }

    findUsers(): Observable<Utilisateur[]> {
        return this.http.get<Utilisateur[]>(API.GET_ALL_Utilisateur)
    }
    findUtilisateurByID(id:number,token:string): Observable<Utilisateur> {
        return this.http.get<Utilisateur>(API.GET_Utilisateur_ID + id + "?token=" + token)
    }
    addUser(user) :Observable<HttpResponse<Utilisateur>>{
        return this.http.put<Utilisateur>(API.ADD_Utilisateur,user,{observe:'response'})
    }

    updateUtilisateurByID(Utilisateur:Utilisateur): Observable<Utilisateur> {    
        return null}

    findImgUtilisateurById(id: number): Observable<Image> {    return this.http.get<Image>(API.GET_IMG_Utilisateur + id)}  
    /*
    updateImgUtilisateurByID(img:Image): Observable<Utilisateur> {    return this.http.patch<Utilisateur>((API.UPDATE_IMG_Utilisateur_ID + img.id ),{
        "imagePath": img.path,
        "image64": img.base64
    })}*/
    deleteUtilisateurById(id: number): Observable<Utilisateur> {    return this.http.delete<Utilisateur>(API.DELETE_Utilisateur_ID + id)}  

    findFakeUtilisateurByID(): Observable<HttpResponse<Utilisateur>> {
        let i: Ingredient = new Ingredient();
        Object.assign(i,{
            "Id":1,
            "Nom": "Pomme",
            "CodeBarre": "",
            "Quantite": 1,
            "Contenant": 1,
            "Unite": 1,
            "Image": null,
            "Categorie": "Fruits",
        })
        let i2: Ingredient = new Ingredient();
        Object.assign(i2,{
            "Id":2,
            "Nom": "Orange",
            "CodeBarre": "",
            "Quantite": 4,
            "Contenant": 4,
            "Unite": 9,
            "Image": null,
            "Categorie": "Fruits",
        })
        let i3: Ingredient = new Ingredient();
        Object.assign(i3,{
            "Id":3,
            "Nom": "Coca cola",
            "CodeBarre": "",
            "Quantite": 2,
            "Contenant": 6,
            "Unite": 9,
            "Image": null,
            "Categorie": "Sodas",
        })
        //const token = generateJWTToken("");
        let u: Utilisateur = new Utilisateur();
        Object.assign(u, {
          "Id": 1,
          "Prenom": "fake",
          "Nom": "M fake",
          "ImageId": 1,
          "MotDePasse": "passe",
          "MonSecret": "secret",
          "Token": "my-fake-token",
          "Image": null,
          "Favoris": null,
          "Frigo": [i,i2,i3,i,i,i2,i2,i3,i3],
          "ListCourse": null
      });
      const response: HttpResponse<Utilisateur> = new HttpResponse({
        status: 200,
        body: u,
      });
      console.log(response);
      // Renvoyez l'observable avec la réponse HTTP
      return new Observable<HttpResponse<Utilisateur>>(observer => {
        observer.next(response); // Émet la réponse
        observer.complete(); // Termine l'observable
      });
    }
}