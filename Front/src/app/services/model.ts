export interface Image {
    Id: number;
    base64: string;
    path: string;
}
export class Ingredient {
    Id:number;
    Nom: string;
    CodeBarre: string;
    Quantite: number;
    Contenant: number;
    Unite: number;
    Image: Image;//changer par img URL
    Categorie: string;
}


export class Utilisateur {
    Id: number;
    Nom: string;
    Prenom: string;
    MonSecret: string;
    token: string;
    MotDePasse: string;
    Image: Image; //changer par img URL
    Favoris: Recette[];
    Frigo: Ingredient[];
    ListCourse: Ingredient[];
}

export interface Recette {
    Id: number;
    Nom: string;
    temps: number;
    personne: number;
    Image: Image;//changer par img URL
}