import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import jwt from 'jsonwebtoken';

@Injectable({
  providedIn: 'root'
})
export class JwtDecodeService {
    
    jwtToken: string;
    decodedToken: { [key: string]: string };
 
    constructor() {
    }
 
    setToken(token: string) {
      if (token) {
        this.jwtToken = token;
      }
    }
 
    decodeToken() {
      if (this.jwtToken) {
      this.decodedToken = jwt_decode(this.jwtToken);
      }
    }
 
    getDecodeToken() {
      return jwt_decode(this.jwtToken);
    }
 
    getUser() {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.displayname : null;
    }
 
    getEmailId() {
      this.decodeToken();
      return this.decodedToken ? this.decodedToken.email : null;
    }
 
    getExpiryTime(): number {
      this.decodeToken();
      return this.decodedToken ? Number.parseInt(this.decodedToken.exp) : null;
    }
  
    isTokenExpired(): boolean {
      const expiryTime: number = this.getExpiryTime();
      if (expiryTime) {
        return ((1000 * expiryTime) - (new Date()).getTime()) <  0 ; // expiryTime default 30 min 
      } else {
        return false;
      }
    }
  
    static generateJWTToken(payload: any): string {
      const secretKey = '1_1_2_3_5_8_13_21_34_55_89_144_233_377_610'; // Remplacez par votre propre clé secrète
      const options = {
        expiresIn: '1h', // Durée de validité du token
      };
    
      const token = jwt.sign(payload, secretKey, options);
      return token;
    }
    


    

}