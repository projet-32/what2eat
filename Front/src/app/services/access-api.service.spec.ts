import { TestBed } from '@angular/core/testing';

import { AccessAPIService } from './access-api.service';

describe('AccessAPIService', () => {
  let service: AccessAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccessAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
