import { Component, OnInit } from '@angular/core';
import { AccessAPIService } from 'src/app/services/access-api.service';
import { Utilisateur, Ingredient } from 'src/app/services/model';
import { JwtDecodeService } from '../../services/jwt-decode.service';

@Component({
  selector: 'app-frigo',
  templateUrl: './frigo.page.html',
  styleUrls: ['./frigo.page.scss'],
})
export class FrigoPage implements OnInit {
  isConnected: boolean = false;
  isSynchro: boolean = true;
  status = 'V'; 
  private utilisateurPage: any;
  private choixCategorie: String;
  public choixingredients: Array<Ingredient> = [];
  userInfo: any;
  
  constructor(private accessApi:AccessAPIService,private jwtDecodeService: JwtDecodeService) {
  }
  ngOnInit() {
    this.checkProfile();
    
    //this.utilisateurPage = this.accessApi.findFakeUtilisateurByID(0);
    //console.log(this.utilisateurPage);
    this.choixCategorie = "Fruits";
    this.updatevue();
  }

  updatevue(){
    this.choixingredients = [];
    this.utilisateurPage.frigo.forEach(elem =>{
      if(elem.categorie==this.choixCategorie){this.choixingredients.push(elem)}
    })
  }
  setchoixCategorie(choix: String){
    this.choixCategorie=choix;
    this.updatevue();
  }
  
  checkProfile() {
    let token = localStorage.getItem('jwtToken');
    this.utilisateurPage = JSON.parse(token);
    console.log(this.utilisateurPage.token);
    if (this.utilisateurPage.token != null && this.utilisateurPage.token) {
      this.jwtDecodeService.setToken(this.utilisateurPage.token);
      this.jwtDecodeService.decodeToken();
      console.log((((1000 * this.jwtDecodeService.getExpiryTime()) - (new Date()).getTime()) )/60000);
      if(this.jwtDecodeService.isTokenExpired()){
        location.href = '/login';
      }
    }else{
      location.href = '/login';
    }
  }
  addQuantite(nom){
    console.log("add + 1 "+ nom);
    this.utilisateurPage.frigo.forEach(elem =>{
      if(elem.nom==nom){elem.quantite = elem.quantite + 1}
    })
    this.falseSyncrho();
    this.updatevue();
  }
  removeQuantite(nom){
    console.log("remove - 1 "+ nom);
    this.utilisateurPage.frigo.forEach(elem =>{
      if(elem.nom==nom){elem.quantite = elem.quantite - 1}
    })
    this.falseSyncrho();
    this.updatevue();
  }
  removeProduit(id){
    console.log("delete "+ this.utilisateurPage.frigo[id-1].nom);
    delete this.utilisateurPage.frigo[id-1];
    this.falseSyncrho();
    this.updatevue();
  }
  synchroUser() {
      if(!this.isSynchro){
        this.accessApi.updateUtilisateurByID(this.utilisateurPage)
        this.trueSyncrho();
      }
  }
  trueSyncrho(){
      this.isSynchro=true;
      this.status="V";
  }
  falseSyncrho(){
    this.isSynchro=false;
    this.status="X";
  }

}
