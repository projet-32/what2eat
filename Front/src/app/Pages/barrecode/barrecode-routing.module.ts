import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BarrecodePage } from './barrecode.page';

const routes: Routes = [
  {
    path: '',
    component: BarrecodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BarrecodePageRoutingModule {}
