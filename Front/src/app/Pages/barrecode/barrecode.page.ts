import { Component, OnInit } from '@angular/core';
import { BarcodeScanner,BarcodeScannerOptions } from '@awesome-cordova-plugins/barcode-scanner/ngx';
@Component({
  selector: 'app-barrecode',
  templateUrl: './barrecode.page.html',
  styleUrls: ['./barrecode.page.scss'],
})
export class BarrecodePage implements OnInit {

  scannedData: any;


  constructor(private barcodeScanner: BarcodeScanner) { 
    this.scannedData = "Aucun barecode";
  }
  scanBarcode() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.scannedData=barcodeData;
     }).catch(err => {
         console.log('Error', err);
     });
    /*
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      formats: 'EAN_13,EAN_8,QR_CODE,PDF_417 ',
      orientation: 'portrait',
    };
    this.barcodeScanner.scan(options).then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.scannedData = barcodeData;

    }).catch(err => {
      console.log('Error', err);
    });*/
  }

  createBarcode() {
    /*
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, this.inputData).then((encodedData) => {
      console.log(encodedData);
      this.encodedData = encodedData;
    }, (err) => {
      console.log('Error occured : ' + err);
    });*/
  }
  ngOnInit() {
  }

}
