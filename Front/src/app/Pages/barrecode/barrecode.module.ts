import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BarrecodePageRoutingModule } from './barrecode-routing.module';

import { BarrecodePage } from './barrecode.page';
import { BarcodeScanner,BarcodeScannerOptions } from '@awesome-cordova-plugins/barcode-scanner/ngx';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BarrecodePageRoutingModule
  ],
  declarations: [BarrecodePage],
  providers: [BarcodeScanner]
})
export class BarrecodePageModule {}
