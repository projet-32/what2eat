import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../services/model';
import { AccessAPIService } from '../services/access-api.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class save implements OnInit {
  utilil: Utilisateur;

  constructor(private mybd: AccessAPIService) {
    this.Get_Utili(1);
  }
  
  Create_Utilisateur(){
    this.utilil={
      "Id": 0,
      "Nom": "Test",
      "Prenom": "",
      "Token": "",
      "Favoris": undefined,
      "Image": undefined,
      "MonSecret": "",
      "MotDePasse": "",
      "Frigo": undefined,
      "ListCourse": undefined
    }
  }

  Get_Utili(e: any){
    console.log("teste Utili");
    let source;
    source = this.mybd.findUtilisateurByID(1).subscribe(
      (resp) => {this.utilil = resp;
        console.log("response");
        console.log(this.utilil);
      }
    );
  }
}
