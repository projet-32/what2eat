import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Utilisateur } from '../../services/model';
import { AccessAPIService } from '../../services/access-api.service';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PasswordValidator } from './password.validator';
@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss']
})
export class LoginPage implements OnInit {

  // les entrées de formulaire connection
  authForm = {
    prenom: "",
    password: ""
  };
  // les entrées de formulaire inscription
  registerForm = {
    "password": "",
    "nom": "",
    "prenom": "",
    "image": {
      "imagePath": "",
      "image64": ""
    }
  };

  forgottenPass = {
    "prenom": ""
  };

  //modal mdp oublié
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    });
  }
  // pour fermer le modal
  closeResult = '';

  // déclaration des formulaires
  forgottenPassword_form: FormGroup;
  auth_form: FormGroup;
  register_form: FormGroup;
  matching_passwords_group: FormGroup;

  constructor(private accessApi: AccessAPIService, private modalService: NgbModal, public formBuilder: FormBuilder) { }

  ngOnInit() {


    // déclaration d'un formulaire de mot de passe identique et ses conditions
    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    },
      (formGroup: FormGroup) => {
        return PasswordValidator.areEqual(formGroup);
      });

    // condition du formulaire mdp oublié
    this.forgottenPassword_form = this.formBuilder.group({
      prenom: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9]+$')
      ]))
    })

    // conditions du formulaire de connection
    this.auth_form = this.formBuilder.group({
      prenom: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9]+$')
      ])),
      password: new FormControl('', Validators.required)
    })

    // conditions du formulaire d'inscription
    this.register_form = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      firstname: new FormControl('', Validators.required),
      prenom: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9]+$')
      ])),
      matching_passwords: this.matching_passwords_group
    })
  }

  // messages d'erreurs trié par champs et par type
  validation_messages = {
    'nom': [
      { type: 'required', message: 'Nom requis.' }
    ],
    'prenom': [
      { type: 'required', message: 'Prénom requis.' }
    ],
    'secret': [
      { type: 'required', message: 'Mot magique requise.' },
      { type: 'pattern', message: 'Entrez une Mot magique valide.' }
    ],
    'password': [
      { type: 'required', message: 'Mot de passe requis.' },
      { type: 'minlength', message: 'Le mot de passe doit avoir au moins 5 caractères.' },
      { type: 'pattern', message: 'Le mot de passe doit contenir au moins une majuscule, une minuscule et un chiffre.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'confirmation du mot de passe requis.' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'mot de passe non-correspondant.' }
    ]
  }

  // Récupération données inscription et envoie de requête
  register(values) {
    this.accessApi.addUser(values).subscribe((res) => {
      if (res.status === 200) {
        alert("inscription réussie");
      }
    })
  }
  // Récupération donnée connection et envoie de requête
  auth(values) {
    this.accessApi.LoginUser(values).subscribe((res) => {
      if (res.status === 200) {
        localStorage.setItem('jwtToken', JSON.stringify(res.body));
        alert("vous êtes connecté");
        location.href = '/frigo';
      }
    })
  }
  // Récupération donnée mdp oublié et envoie de requête
  sendForgottenPass(value) {
    this.accessApi.GetForgetPass(value);
    alert("un prenom vous a été envoyé");
  }
}
