import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./Pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'frigo',
    loadChildren: () => import('./Pages/frigo/frigo.module').then( m => m.FrigoPageModule)
  },
  {
    path: '',
    loadChildren: () => import('./Pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'scan',
    loadChildren: () => import('./Pages/scan/scan.module').then( m => m.ScanPageModule)
  },
  {
    path: 'barrecode',
    loadChildren: () => import('./Pages/barrecode/barrecode.module').then( m => m.BarrecodePageModule)
  },




];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
