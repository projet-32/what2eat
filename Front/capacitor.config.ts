import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'what2eat',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
